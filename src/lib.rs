use rand::Rng;
use std::io;

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum Action {
    Kryten,
    Rimmer,
    Cat,
    Dave,
    Holy,
}

impl std::str::FromStr for Action {
    type Err = ();

    fn from_str(s: &str) -> std::result::Result<Action,()> {
        match s {
            "kryten" => Ok(Action::Kryten),
            "rimmer" => Ok(Action::Rimmer),
            "cat" => Ok(Action::Cat),
            "dave" => Ok(Action::Dave),
            "holy" => Ok(Action::Holy),
            _ => Err(()),
        }
    }
}

pub enum Result {
    Win,
    Lose,
    Tie,
}

impl Result {
pub    fn as_str(&self) -> &'static str {
        match self {
            Result::Win => "You win!",
            Result::Lose => "You lose!",
            Result::Tie => "It's a tie!",
        }
    }
}

pub fn get_player_action() -> Action {
    loop {
        let mut input = String::new();
        println!("Enter you action (kryten, rimmer, cat, dave, or holy):");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let action = input.trim().to_lowercase().parse();
        if let Ok(action) = action {
            return action;
        }
        println!("Invalid action. Please try again.");
    }
}

pub fn get_ai_action() -> Action {
    let actions = [
        Action::Kryten,
        Action::Rimmer,
        Action::Cat,
        Action::Dave,
        Action::Holy,
    ];
    let index = rand::thread_rng().gen_range(0..actions.len());
    actions[index]
}

pub fn get_result(player_action: Action, ai_action: Action) -> Result {
    if player_action == ai_action {
        Result::Tie
    } else if (player_action == Action::Kryten && (ai_action == Action::Rimmer || ai_action == Action::Dave))
        || (player_action == Action::Rimmer && (ai_action == Action::Cat || ai_action == Action::Holy))
        || (player_action == Action::Cat && (ai_action == Action::Dave || ai_action == Action::Kryten))
        || (player_action == Action::Dave && (ai_action == Action::Holy || ai_action == Action::Rimmer))
        || (player_action == Action::Holy && (ai_action == Action::Kryten || ai_action == Action::Cat))
        {
            Result::Win
        } else {
            Result::Lose
        }
}
