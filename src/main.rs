use rock_paper_scissors_lizard_spock::{get_player_action, get_ai_action, get_result, Result};

fn main() {
    let mut player_wins = 0;
    let mut ai_wins = 0;

    println!("Welcome to Rock-Paper-Scissors-Lizard-Spock with Red Dwarf characters!");
    println!("You will be playing against an AI that chooses actions randomly.");
    println!("The first player to win 3 games wins the match.");
    println!("");

    loop {
        let player_action = get_player_action();
        let ai_action = get_ai_action();
        let result = get_result(player_action, ai_action);

        println!("You chose {:#?}.", player_action);

        println!("The AI chose {:#?}.", ai_action);
        println!("{}", result.as_str());

        match result {
            Result::Win => player_wins += 1,
            Result::Lose => ai_wins += 1,
            Result::Tie => (),
        }

        println!("Player wins: {}", player_wins);
        println!("AI wins: {}", ai_wins);
        println!("");

        if player_wins >= 3 {
            println!("Congratulations: You won the match!");
            break;
        } else if ai_wins >= 3 {
            println!("Sorry, you lost the match. Better luck next time!");
            break;
        }
    }
}
